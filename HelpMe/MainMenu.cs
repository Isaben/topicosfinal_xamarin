﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Support.V7.Widget;

namespace HelpMe {
    public class MainMenu {

        Picture[] calculo1;
        Picture[] fisica1;
        Picture[] principal;
        Picture[] calculo2;
        Picture[] eletromagnetismo;
        Picture[] circuitos;

        public static Picture[] MenuPrincipal = {
            new Picture(Resource.Drawable.calculo1, "Calculo 1"),
            new Picture(Resource.Drawable.fisica1, "Física 1"),
            new Picture(Resource.Drawable.eletromagnetismo, "Eletromagnetismo"),
            new Picture(Resource.Drawable.calculo2, "Cálculo 2"),
            new Picture(Resource.Drawable.circuitos, "Circuitos Elétricos")
        };

        public static Picture[] MenuCalculo1 = {
            new Picture(Resource.Drawable.continuidade, "Continuidade de Funções"),
            new Picture(Resource.Drawable.limite, "Limites"),
            new Picture(Resource.Drawable.derivadas_regras, "Derivadas Regras"),
            new Picture(Resource.Drawable.derivada_definicao, "Derivadas Definição"),
            new Picture(Resource.Drawable.integral_definicao, "Integral Definição"),
        };

        static Picture[] MenuCalculo2 = {
            new Picture(Resource.Drawable.integral_aplicacao, "Integral Aplicação"),
            new Picture(Resource.Drawable.integral_regras, "Integral Regras"),
            new Picture(Resource.Drawable.polares, "Coordenadas Polares"),
        };

        static Picture[] MenuFisica1 = {
            new Picture(Resource.Drawable.rotacao, "Rotação"),
            new Picture(Resource.Drawable.vetores, "Vetores"),
            new Picture(Resource.Drawable.forca, "Força"),
            new Picture(Resource.Drawable.cinematica, "Cinemática"),
        };

        static Picture[] MenuEletromagnetismo = {
            new Picture(Resource.Drawable.coulomb, "Lei de Coulomb"),
            new Picture(Resource.Drawable.gauss, "Lei de Gauss"),
            new Picture(Resource.Drawable.lorentz, "Força de Lorentz"),
            new Picture(Resource.Drawable.savart, "Lei de Biot-Savart")
        };

        static Picture[] MenuCircuitos = {
            new Picture(Resource.Drawable.rl, "Circuitos RL"),
            new Picture(Resource.Drawable.rc, "Circuitos RC"),
            new Picture(Resource.Drawable.nos, "Lei dos Nós"),
            new Picture(Resource.Drawable.malhas, "Lei das Malhas"),
            new Picture(Resource.Drawable.resistivo, "Circuitos Resistivos")
        };

        public Picture[] Principal {
            get {
                return principal;
            }
        }

        public Picture[] Fisica1 {
            get {
                return fisica1;
            }
        }

        public Picture[] Calculo1 {
            get {
                return calculo1;
            }
        }

        public Picture[] Eletromagnetismo {
            get {
                return eletromagnetismo;
            }
        }

        public Picture[] Calculo2 {
            get {
                return calculo2;
            }
        }

        public Picture[] Circuitos {
            get {
                return circuitos;
            }
        }

        public MainMenu() {
            calculo1 = MenuCalculo1;
            calculo2 = MenuCalculo2;
            fisica1 = MenuFisica1;
            eletromagnetismo = MenuEletromagnetismo;
            circuitos = MenuCircuitos;
            principal = MenuPrincipal;
        }
    }
}