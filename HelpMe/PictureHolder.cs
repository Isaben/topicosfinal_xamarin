using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Support.V7.Widget;
using Android.Support.V7.RecyclerView;
using Android.Support.V4.Widget;

namespace HelpMe {
    public class PictureHolder : RecyclerView.ViewHolder{

        public ImageView Imagem {
            get;
            private set;
        }
        public TextView Legenda {
            get;
            private set;
        }

        public PictureHolder(View itemView, Action<int> listener) : base (itemView) {

            Imagem = itemView.FindViewById<ImageView>(Resource.Id.imageView);
            Legenda = itemView.FindViewById<TextView>(Resource.Id.textView);

            itemView.Click += (sender, e) => listener(Position);
        }
    }
}