using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Support.V7.Widget;
using Android.Support.V7.RecyclerView;
using Android.Support.V4.Widget;

namespace HelpMe {
    public class PictureAdapter : RecyclerView.Adapter {

        public event EventHandler<int> ItemClick;
        public MainMenu data;
        
        public PictureAdapter(MainMenu Data) {
            data = Data;
        }

        public override RecyclerView.ViewHolder OnCreateViewHolder(ViewGroup parent, int viewType) {

            View itemView = LayoutInflater.From(parent.Context).Inflate(Resource.Layout.MenuCardView, parent, false);

            PictureHolder ph = new PictureHolder(itemView, OnClick);
            return ph;
        }

        public override void OnBindViewHolder(RecyclerView.ViewHolder holder, int position) {
            PictureHolder ph = holder as PictureHolder;

            ph.Imagem.SetImageResource(data.Principal[position].id);
            ph.Legenda.Text = data.Principal[position].legenda;
        }

        public override int ItemCount {
            get {
                return data.Principal.Length;
            }
        }

        void OnClick(int position) {
            ItemClick?.Invoke(this, position);
        }

    }
}