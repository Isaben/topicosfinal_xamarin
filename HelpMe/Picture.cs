﻿namespace HelpMe {
    public class Picture {

        public int id {
            get;
            private set;
        }
        public string legenda {
            get;
            private set;
        }

        public Picture(int id, string legenda) {
            this.id = id;
            this.legenda = legenda;
        }
    }
}