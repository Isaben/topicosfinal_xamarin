﻿using System;
using Android.App;
using Android.Content;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;
using Android.Support.V7.Widget;
using Android.Support.V7.RecyclerView;
using Android.Support.V4.Widget;
using Android.Support.V4.App;

namespace HelpMe {
    [Activity(Label = "HelpMe", MainLauncher = true, Icon = "@drawable/icon", Theme = "@android:style/Theme.Material.Light.DarkActionBar")]
    public class MainActivity : Activity {
        MainMenu menu;
        RecyclerView cards;
        RecyclerView.LayoutManager layout;
        PictureAdapter adapter;
        

        protected override void OnCreate(Bundle bundle) {
            base.OnCreate(bundle);
            SetContentView(Resource.Layout.Main);
            menu = new MainMenu();
            cards = FindViewById<RecyclerView>(Resource.Id.menu_principal);
            if(cards == null) {
                SetContentView(Resource.Layout.layout1);
                return;
            }

            layout = new LinearLayoutManager(BaseContext);
            cards.SetLayoutManager(layout);

            adapter = new PictureAdapter(menu);
            adapter.ItemClick += OnItemClick;
            cards.SetAdapter(adapter);
            // Set our view from the "main" layout resource
            
            
            
        }

        void OnItemClick(object sender, int position) {
            int photoNum = position + 1;
            Toast.MakeText(this, "This is photo number " + photoNum, ToastLength.Short).Show();
        }
    }
}

