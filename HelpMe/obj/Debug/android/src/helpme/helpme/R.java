/* AUTO-GENERATED FILE.  DO NOT MODIFY.
 *
 * This class was automatically generated by the
 * aapt tool from the resource data it found.  It
 * should not be modified by hand.
 */

package helpme.helpme;

public final class R {
    public static final class attr {
        /** <p>Must be a color value, in the form of "<code>#<i>rgb</i></code>", "<code>#<i>argb</i></code>",
"<code>#<i>rrggbb</i></code>", or "<code>#<i>aarrggbb</i></code>".
<p>This may also be a reference to a resource (in the form
"<code>@[<i>package</i>:]<i>type</i>:<i>name</i></code>") or
theme attribute (in the form
"<code>?[<i>package</i>:][<i>type</i>:]<i>name</i></code>")
containing a value of this type.
         */
        public static final int cardBackgroundColor=0x7f010004;
        /** <p>Must be a dimension value, which is a floating point number appended with a unit such as "<code>14.5sp</code>".
Available units are: px (pixels), dp (density-independent pixels), sp (scaled pixels based on preferred font size),
in (inches), mm (millimeters).
<p>This may also be a reference to a resource (in the form
"<code>@[<i>package</i>:]<i>type</i>:<i>name</i></code>") or
theme attribute (in the form
"<code>?[<i>package</i>:][<i>type</i>:]<i>name</i></code>")
containing a value of this type.
         */
        public static final int cardCornerRadius=0x7f010005;
        /** <p>Must be a dimension value, which is a floating point number appended with a unit such as "<code>14.5sp</code>".
Available units are: px (pixels), dp (density-independent pixels), sp (scaled pixels based on preferred font size),
in (inches), mm (millimeters).
<p>This may also be a reference to a resource (in the form
"<code>@[<i>package</i>:]<i>type</i>:<i>name</i></code>") or
theme attribute (in the form
"<code>?[<i>package</i>:][<i>type</i>:]<i>name</i></code>")
containing a value of this type.
         */
        public static final int cardElevation=0x7f010006;
        /** <p>Must be a dimension value, which is a floating point number appended with a unit such as "<code>14.5sp</code>".
Available units are: px (pixels), dp (density-independent pixels), sp (scaled pixels based on preferred font size),
in (inches), mm (millimeters).
<p>This may also be a reference to a resource (in the form
"<code>@[<i>package</i>:]<i>type</i>:<i>name</i></code>") or
theme attribute (in the form
"<code>?[<i>package</i>:][<i>type</i>:]<i>name</i></code>")
containing a value of this type.
         */
        public static final int cardMaxElevation=0x7f010007;
        /** <p>Must be a boolean value, either "<code>true</code>" or "<code>false</code>".
<p>This may also be a reference to a resource (in the form
"<code>@[<i>package</i>:]<i>type</i>:<i>name</i></code>") or
theme attribute (in the form
"<code>?[<i>package</i>:][<i>type</i>:]<i>name</i></code>")
containing a value of this type.
         */
        public static final int cardPreventCornerOverlap=0x7f010009;
        /** <p>Must be a boolean value, either "<code>true</code>" or "<code>false</code>".
<p>This may also be a reference to a resource (in the form
"<code>@[<i>package</i>:]<i>type</i>:<i>name</i></code>") or
theme attribute (in the form
"<code>?[<i>package</i>:][<i>type</i>:]<i>name</i></code>")
containing a value of this type.
         */
        public static final int cardUseCompatPadding=0x7f010008;
        /** <p>Must be a dimension value, which is a floating point number appended with a unit such as "<code>14.5sp</code>".
Available units are: px (pixels), dp (density-independent pixels), sp (scaled pixels based on preferred font size),
in (inches), mm (millimeters).
<p>This may also be a reference to a resource (in the form
"<code>@[<i>package</i>:]<i>type</i>:<i>name</i></code>") or
theme attribute (in the form
"<code>?[<i>package</i>:][<i>type</i>:]<i>name</i></code>")
containing a value of this type.
         */
        public static final int contentPadding=0x7f01000a;
        /** <p>Must be a dimension value, which is a floating point number appended with a unit such as "<code>14.5sp</code>".
Available units are: px (pixels), dp (density-independent pixels), sp (scaled pixels based on preferred font size),
in (inches), mm (millimeters).
<p>This may also be a reference to a resource (in the form
"<code>@[<i>package</i>:]<i>type</i>:<i>name</i></code>") or
theme attribute (in the form
"<code>?[<i>package</i>:][<i>type</i>:]<i>name</i></code>")
containing a value of this type.
         */
        public static final int contentPaddingBottom=0x7f01000e;
        /** <p>Must be a dimension value, which is a floating point number appended with a unit such as "<code>14.5sp</code>".
Available units are: px (pixels), dp (density-independent pixels), sp (scaled pixels based on preferred font size),
in (inches), mm (millimeters).
<p>This may also be a reference to a resource (in the form
"<code>@[<i>package</i>:]<i>type</i>:<i>name</i></code>") or
theme attribute (in the form
"<code>?[<i>package</i>:][<i>type</i>:]<i>name</i></code>")
containing a value of this type.
         */
        public static final int contentPaddingLeft=0x7f01000b;
        /** <p>Must be a dimension value, which is a floating point number appended with a unit such as "<code>14.5sp</code>".
Available units are: px (pixels), dp (density-independent pixels), sp (scaled pixels based on preferred font size),
in (inches), mm (millimeters).
<p>This may also be a reference to a resource (in the form
"<code>@[<i>package</i>:]<i>type</i>:<i>name</i></code>") or
theme attribute (in the form
"<code>?[<i>package</i>:][<i>type</i>:]<i>name</i></code>")
containing a value of this type.
         */
        public static final int contentPaddingRight=0x7f01000c;
        /** <p>Must be a dimension value, which is a floating point number appended with a unit such as "<code>14.5sp</code>".
Available units are: px (pixels), dp (density-independent pixels), sp (scaled pixels based on preferred font size),
in (inches), mm (millimeters).
<p>This may also be a reference to a resource (in the form
"<code>@[<i>package</i>:]<i>type</i>:<i>name</i></code>") or
theme attribute (in the form
"<code>?[<i>package</i>:][<i>type</i>:]<i>name</i></code>")
containing a value of this type.
         */
        public static final int contentPaddingTop=0x7f01000d;
        /** <p>Must be a string value, using '\\;' to escape characters such as '\\n' or '\\uxxxx' for a unicode character.
<p>This may also be a reference to a resource (in the form
"<code>@[<i>package</i>:]<i>type</i>:<i>name</i></code>") or
theme attribute (in the form
"<code>?[<i>package</i>:][<i>type</i>:]<i>name</i></code>")
containing a value of this type.
         */
        public static final int layoutManager=0x7f010000;
        /** <p>Must be a boolean value, either "<code>true</code>" or "<code>false</code>".
<p>This may also be a reference to a resource (in the form
"<code>@[<i>package</i>:]<i>type</i>:<i>name</i></code>") or
theme attribute (in the form
"<code>?[<i>package</i>:][<i>type</i>:]<i>name</i></code>")
containing a value of this type.
         */
        public static final int reverseLayout=0x7f010002;
        /** <p>Must be an integer value, such as "<code>100</code>".
<p>This may also be a reference to a resource (in the form
"<code>@[<i>package</i>:]<i>type</i>:<i>name</i></code>") or
theme attribute (in the form
"<code>?[<i>package</i>:][<i>type</i>:]<i>name</i></code>")
containing a value of this type.
         */
        public static final int spanCount=0x7f010001;
        /** <p>Must be a boolean value, either "<code>true</code>" or "<code>false</code>".
<p>This may also be a reference to a resource (in the form
"<code>@[<i>package</i>:]<i>type</i>:<i>name</i></code>") or
theme attribute (in the form
"<code>?[<i>package</i>:][<i>type</i>:]<i>name</i></code>")
containing a value of this type.
         */
        public static final int stackFromEnd=0x7f010003;
    }
    public static final class color {
        public static final int cardview_dark_background=0x7f060000;
        public static final int cardview_light_background=0x7f060001;
        public static final int cardview_shadow_end_color=0x7f060002;
        public static final int cardview_shadow_start_color=0x7f060003;
    }
    public static final class dimen {
        public static final int cardview_compat_inset_shadow=0x7f040001;
        public static final int cardview_default_elevation=0x7f040002;
        public static final int cardview_default_radius=0x7f040003;
        public static final int item_touch_helper_max_drag_scroll_per_frame=0x7f040000;
    }
    public static final class drawable {
        public static final int calculo1=0x7f020000;
        public static final int calculo2=0x7f020001;
        public static final int cinematica=0x7f020002;
        public static final int circuitos=0x7f020003;
        public static final int continuidade=0x7f020004;
        public static final int coulomb=0x7f020005;
        public static final int derivada_definicao=0x7f020006;
        public static final int derivadas_regras=0x7f020007;
        public static final int eletromagnetismo=0x7f020008;
        public static final int fisica1=0x7f020009;
        public static final int forca=0x7f02000a;
        public static final int gauss=0x7f02000b;
        public static final int icon=0x7f02000c;
        public static final int integral_aplicacao=0x7f02000d;
        public static final int integral_definicao=0x7f02000e;
        public static final int integral_regras=0x7f02000f;
        public static final int limite=0x7f020010;
        public static final int lorentz=0x7f020011;
        public static final int malhas=0x7f020012;
        public static final int nos=0x7f020013;
        public static final int polares=0x7f020014;
        public static final int rc=0x7f020015;
        public static final int resistivo=0x7f020016;
        public static final int rl=0x7f020017;
        public static final int rotacao=0x7f020018;
        public static final int savart=0x7f020019;
        public static final int vetores=0x7f02001a;
    }
    public static final class id {
        public static final int imageView=0x7f050002;
        public static final int item_touch_helper_previous_elevation=0x7f050000;
        public static final int menu_principal=0x7f050001;
        public static final int textView=0x7f050003;
    }
    public static final class layout {
        public static final int layout1=0x7f030000;
        public static final int main=0x7f030001;
        public static final int menucardview=0x7f030002;
    }
    public static final class string {
        public static final int ApplicationName=0x7f080001;
        public static final int Hello=0x7f080000;
    }
    public static final class style {
        public static final int CardView=0x7f070000;
        public static final int CardView_Dark=0x7f070001;
        public static final int CardView_Light=0x7f070002;
    }
    public static final class styleable {
        /** Attributes that can be used with a CardView.
           <p>Includes the following attributes:</p>
           <table>
           <colgroup align="left" />
           <colgroup align="left" />
           <tr><th>Attribute</th><th>Description</th></tr>
           <tr><td><code>{@link #CardView_cardBackgroundColor HelpMe.HelpMe:cardBackgroundColor}</code></td><td></td></tr>
           <tr><td><code>{@link #CardView_cardCornerRadius HelpMe.HelpMe:cardCornerRadius}</code></td><td></td></tr>
           <tr><td><code>{@link #CardView_cardElevation HelpMe.HelpMe:cardElevation}</code></td><td></td></tr>
           <tr><td><code>{@link #CardView_cardMaxElevation HelpMe.HelpMe:cardMaxElevation}</code></td><td></td></tr>
           <tr><td><code>{@link #CardView_cardPreventCornerOverlap HelpMe.HelpMe:cardPreventCornerOverlap}</code></td><td></td></tr>
           <tr><td><code>{@link #CardView_cardUseCompatPadding HelpMe.HelpMe:cardUseCompatPadding}</code></td><td></td></tr>
           <tr><td><code>{@link #CardView_contentPadding HelpMe.HelpMe:contentPadding}</code></td><td></td></tr>
           <tr><td><code>{@link #CardView_contentPaddingBottom HelpMe.HelpMe:contentPaddingBottom}</code></td><td></td></tr>
           <tr><td><code>{@link #CardView_contentPaddingLeft HelpMe.HelpMe:contentPaddingLeft}</code></td><td></td></tr>
           <tr><td><code>{@link #CardView_contentPaddingRight HelpMe.HelpMe:contentPaddingRight}</code></td><td></td></tr>
           <tr><td><code>{@link #CardView_contentPaddingTop HelpMe.HelpMe:contentPaddingTop}</code></td><td></td></tr>
           </table>
           @see #CardView_cardBackgroundColor
           @see #CardView_cardCornerRadius
           @see #CardView_cardElevation
           @see #CardView_cardMaxElevation
           @see #CardView_cardPreventCornerOverlap
           @see #CardView_cardUseCompatPadding
           @see #CardView_contentPadding
           @see #CardView_contentPaddingBottom
           @see #CardView_contentPaddingLeft
           @see #CardView_contentPaddingRight
           @see #CardView_contentPaddingTop
         */
        public static final int[] CardView = {
            0x7f010004, 0x7f010005, 0x7f010006, 0x7f010007,
            0x7f010008, 0x7f010009, 0x7f01000a, 0x7f01000b,
            0x7f01000c, 0x7f01000d, 0x7f01000e
        };
        /**
          <p>This symbol is the offset where the {@link HelpMe.HelpMe.R.attr#cardBackgroundColor}
          attribute's value can be found in the {@link #CardView} array.


          <p>Must be a color value, in the form of "<code>#<i>rgb</i></code>", "<code>#<i>argb</i></code>",
"<code>#<i>rrggbb</i></code>", or "<code>#<i>aarrggbb</i></code>".
<p>This may also be a reference to a resource (in the form
"<code>@[<i>package</i>:]<i>type</i>:<i>name</i></code>") or
theme attribute (in the form
"<code>?[<i>package</i>:][<i>type</i>:]<i>name</i></code>")
containing a value of this type.
          @attr name HelpMe.HelpMe:cardBackgroundColor
        */
        public static final int CardView_cardBackgroundColor = 0;
        /**
          <p>This symbol is the offset where the {@link HelpMe.HelpMe.R.attr#cardCornerRadius}
          attribute's value can be found in the {@link #CardView} array.


          <p>Must be a dimension value, which is a floating point number appended with a unit such as "<code>14.5sp</code>".
Available units are: px (pixels), dp (density-independent pixels), sp (scaled pixels based on preferred font size),
in (inches), mm (millimeters).
<p>This may also be a reference to a resource (in the form
"<code>@[<i>package</i>:]<i>type</i>:<i>name</i></code>") or
theme attribute (in the form
"<code>?[<i>package</i>:][<i>type</i>:]<i>name</i></code>")
containing a value of this type.
          @attr name HelpMe.HelpMe:cardCornerRadius
        */
        public static final int CardView_cardCornerRadius = 1;
        /**
          <p>This symbol is the offset where the {@link HelpMe.HelpMe.R.attr#cardElevation}
          attribute's value can be found in the {@link #CardView} array.


          <p>Must be a dimension value, which is a floating point number appended with a unit such as "<code>14.5sp</code>".
Available units are: px (pixels), dp (density-independent pixels), sp (scaled pixels based on preferred font size),
in (inches), mm (millimeters).
<p>This may also be a reference to a resource (in the form
"<code>@[<i>package</i>:]<i>type</i>:<i>name</i></code>") or
theme attribute (in the form
"<code>?[<i>package</i>:][<i>type</i>:]<i>name</i></code>")
containing a value of this type.
          @attr name HelpMe.HelpMe:cardElevation
        */
        public static final int CardView_cardElevation = 2;
        /**
          <p>This symbol is the offset where the {@link HelpMe.HelpMe.R.attr#cardMaxElevation}
          attribute's value can be found in the {@link #CardView} array.


          <p>Must be a dimension value, which is a floating point number appended with a unit such as "<code>14.5sp</code>".
Available units are: px (pixels), dp (density-independent pixels), sp (scaled pixels based on preferred font size),
in (inches), mm (millimeters).
<p>This may also be a reference to a resource (in the form
"<code>@[<i>package</i>:]<i>type</i>:<i>name</i></code>") or
theme attribute (in the form
"<code>?[<i>package</i>:][<i>type</i>:]<i>name</i></code>")
containing a value of this type.
          @attr name HelpMe.HelpMe:cardMaxElevation
        */
        public static final int CardView_cardMaxElevation = 3;
        /**
          <p>This symbol is the offset where the {@link HelpMe.HelpMe.R.attr#cardPreventCornerOverlap}
          attribute's value can be found in the {@link #CardView} array.


          <p>Must be a boolean value, either "<code>true</code>" or "<code>false</code>".
<p>This may also be a reference to a resource (in the form
"<code>@[<i>package</i>:]<i>type</i>:<i>name</i></code>") or
theme attribute (in the form
"<code>?[<i>package</i>:][<i>type</i>:]<i>name</i></code>")
containing a value of this type.
          @attr name HelpMe.HelpMe:cardPreventCornerOverlap
        */
        public static final int CardView_cardPreventCornerOverlap = 5;
        /**
          <p>This symbol is the offset where the {@link HelpMe.HelpMe.R.attr#cardUseCompatPadding}
          attribute's value can be found in the {@link #CardView} array.


          <p>Must be a boolean value, either "<code>true</code>" or "<code>false</code>".
<p>This may also be a reference to a resource (in the form
"<code>@[<i>package</i>:]<i>type</i>:<i>name</i></code>") or
theme attribute (in the form
"<code>?[<i>package</i>:][<i>type</i>:]<i>name</i></code>")
containing a value of this type.
          @attr name HelpMe.HelpMe:cardUseCompatPadding
        */
        public static final int CardView_cardUseCompatPadding = 4;
        /**
          <p>This symbol is the offset where the {@link HelpMe.HelpMe.R.attr#contentPadding}
          attribute's value can be found in the {@link #CardView} array.


          <p>Must be a dimension value, which is a floating point number appended with a unit such as "<code>14.5sp</code>".
Available units are: px (pixels), dp (density-independent pixels), sp (scaled pixels based on preferred font size),
in (inches), mm (millimeters).
<p>This may also be a reference to a resource (in the form
"<code>@[<i>package</i>:]<i>type</i>:<i>name</i></code>") or
theme attribute (in the form
"<code>?[<i>package</i>:][<i>type</i>:]<i>name</i></code>")
containing a value of this type.
          @attr name HelpMe.HelpMe:contentPadding
        */
        public static final int CardView_contentPadding = 6;
        /**
          <p>This symbol is the offset where the {@link HelpMe.HelpMe.R.attr#contentPaddingBottom}
          attribute's value can be found in the {@link #CardView} array.


          <p>Must be a dimension value, which is a floating point number appended with a unit such as "<code>14.5sp</code>".
Available units are: px (pixels), dp (density-independent pixels), sp (scaled pixels based on preferred font size),
in (inches), mm (millimeters).
<p>This may also be a reference to a resource (in the form
"<code>@[<i>package</i>:]<i>type</i>:<i>name</i></code>") or
theme attribute (in the form
"<code>?[<i>package</i>:][<i>type</i>:]<i>name</i></code>")
containing a value of this type.
          @attr name HelpMe.HelpMe:contentPaddingBottom
        */
        public static final int CardView_contentPaddingBottom = 10;
        /**
          <p>This symbol is the offset where the {@link HelpMe.HelpMe.R.attr#contentPaddingLeft}
          attribute's value can be found in the {@link #CardView} array.


          <p>Must be a dimension value, which is a floating point number appended with a unit such as "<code>14.5sp</code>".
Available units are: px (pixels), dp (density-independent pixels), sp (scaled pixels based on preferred font size),
in (inches), mm (millimeters).
<p>This may also be a reference to a resource (in the form
"<code>@[<i>package</i>:]<i>type</i>:<i>name</i></code>") or
theme attribute (in the form
"<code>?[<i>package</i>:][<i>type</i>:]<i>name</i></code>")
containing a value of this type.
          @attr name HelpMe.HelpMe:contentPaddingLeft
        */
        public static final int CardView_contentPaddingLeft = 7;
        /**
          <p>This symbol is the offset where the {@link HelpMe.HelpMe.R.attr#contentPaddingRight}
          attribute's value can be found in the {@link #CardView} array.


          <p>Must be a dimension value, which is a floating point number appended with a unit such as "<code>14.5sp</code>".
Available units are: px (pixels), dp (density-independent pixels), sp (scaled pixels based on preferred font size),
in (inches), mm (millimeters).
<p>This may also be a reference to a resource (in the form
"<code>@[<i>package</i>:]<i>type</i>:<i>name</i></code>") or
theme attribute (in the form
"<code>?[<i>package</i>:][<i>type</i>:]<i>name</i></code>")
containing a value of this type.
          @attr name HelpMe.HelpMe:contentPaddingRight
        */
        public static final int CardView_contentPaddingRight = 8;
        /**
          <p>This symbol is the offset where the {@link HelpMe.HelpMe.R.attr#contentPaddingTop}
          attribute's value can be found in the {@link #CardView} array.


          <p>Must be a dimension value, which is a floating point number appended with a unit such as "<code>14.5sp</code>".
Available units are: px (pixels), dp (density-independent pixels), sp (scaled pixels based on preferred font size),
in (inches), mm (millimeters).
<p>This may also be a reference to a resource (in the form
"<code>@[<i>package</i>:]<i>type</i>:<i>name</i></code>") or
theme attribute (in the form
"<code>?[<i>package</i>:][<i>type</i>:]<i>name</i></code>")
containing a value of this type.
          @attr name HelpMe.HelpMe:contentPaddingTop
        */
        public static final int CardView_contentPaddingTop = 9;
        /** Attributes that can be used with a RecyclerView.
           <p>Includes the following attributes:</p>
           <table>
           <colgroup align="left" />
           <colgroup align="left" />
           <tr><th>Attribute</th><th>Description</th></tr>
           <tr><td><code>{@link #RecyclerView_android_orientation android:orientation}</code></td><td></td></tr>
           <tr><td><code>{@link #RecyclerView_layoutManager HelpMe.HelpMe:layoutManager}</code></td><td></td></tr>
           <tr><td><code>{@link #RecyclerView_reverseLayout HelpMe.HelpMe:reverseLayout}</code></td><td></td></tr>
           <tr><td><code>{@link #RecyclerView_spanCount HelpMe.HelpMe:spanCount}</code></td><td></td></tr>
           <tr><td><code>{@link #RecyclerView_stackFromEnd HelpMe.HelpMe:stackFromEnd}</code></td><td></td></tr>
           </table>
           @see #RecyclerView_android_orientation
           @see #RecyclerView_layoutManager
           @see #RecyclerView_reverseLayout
           @see #RecyclerView_spanCount
           @see #RecyclerView_stackFromEnd
         */
        public static final int[] RecyclerView = {
            0x010100c4, 0x7f010000, 0x7f010001, 0x7f010002,
            0x7f010003
        };
        /**
          <p>This symbol is the offset where the {@link android.R.attr#orientation}
          attribute's value can be found in the {@link #RecyclerView} array.
          @attr name android:orientation
        */
        public static final int RecyclerView_android_orientation = 0;
        /**
          <p>This symbol is the offset where the {@link HelpMe.HelpMe.R.attr#layoutManager}
          attribute's value can be found in the {@link #RecyclerView} array.


          <p>Must be a string value, using '\\;' to escape characters such as '\\n' or '\\uxxxx' for a unicode character.
<p>This may also be a reference to a resource (in the form
"<code>@[<i>package</i>:]<i>type</i>:<i>name</i></code>") or
theme attribute (in the form
"<code>?[<i>package</i>:][<i>type</i>:]<i>name</i></code>")
containing a value of this type.
          @attr name HelpMe.HelpMe:layoutManager
        */
        public static final int RecyclerView_layoutManager = 1;
        /**
          <p>This symbol is the offset where the {@link HelpMe.HelpMe.R.attr#reverseLayout}
          attribute's value can be found in the {@link #RecyclerView} array.


          <p>Must be a boolean value, either "<code>true</code>" or "<code>false</code>".
<p>This may also be a reference to a resource (in the form
"<code>@[<i>package</i>:]<i>type</i>:<i>name</i></code>") or
theme attribute (in the form
"<code>?[<i>package</i>:][<i>type</i>:]<i>name</i></code>")
containing a value of this type.
          @attr name HelpMe.HelpMe:reverseLayout
        */
        public static final int RecyclerView_reverseLayout = 3;
        /**
          <p>This symbol is the offset where the {@link HelpMe.HelpMe.R.attr#spanCount}
          attribute's value can be found in the {@link #RecyclerView} array.


          <p>Must be an integer value, such as "<code>100</code>".
<p>This may also be a reference to a resource (in the form
"<code>@[<i>package</i>:]<i>type</i>:<i>name</i></code>") or
theme attribute (in the form
"<code>?[<i>package</i>:][<i>type</i>:]<i>name</i></code>")
containing a value of this type.
          @attr name HelpMe.HelpMe:spanCount
        */
        public static final int RecyclerView_spanCount = 2;
        /**
          <p>This symbol is the offset where the {@link HelpMe.HelpMe.R.attr#stackFromEnd}
          attribute's value can be found in the {@link #RecyclerView} array.


          <p>Must be a boolean value, either "<code>true</code>" or "<code>false</code>".
<p>This may also be a reference to a resource (in the form
"<code>@[<i>package</i>:]<i>type</i>:<i>name</i></code>") or
theme attribute (in the form
"<code>?[<i>package</i>:][<i>type</i>:]<i>name</i></code>")
containing a value of this type.
          @attr name HelpMe.HelpMe:stackFromEnd
        */
        public static final int RecyclerView_stackFromEnd = 4;
    };
}
